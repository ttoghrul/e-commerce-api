package com.ecommerce.api

import io.ktor.client.* // ktlint-disable no-wildcard-imports
import io.ktor.client.engine.okhttp.* // ktlint-disable no-wildcard-imports
import io.ktor.client.features.* // ktlint-disable no-wildcard-imports
import io.ktor.client.features.json.* // ktlint-disable no-wildcard-imports
import io.ktor.client.request.* // ktlint-disable no-wildcard-imports
import io.ktor.client.statement.* // ktlint-disable no-wildcard-imports
import io.ktor.http.* // ktlint-disable no-wildcard-imports
import io.ktor.server.engine.* // ktlint-disable no-wildcard-imports
import io.ktor.server.netty.* // ktlint-disable no-wildcard-imports
import kotlinx.coroutines.runBlocking
import org.apache.logging.log4j.LogManager
import org.junit.jupiter.api.* // ktlint-disable no-wildcard-imports
import org.junit.jupiter.api.Assertions.assertEquals
import org.skyscreamer.jsonassert.JSONAssert
import org.skyscreamer.jsonassert.JSONCompareMode

class APIRunnerTest {

    private val client = HttpClient(OkHttp) {
        engine {
            config {
                retryOnConnectionFailure(true)
            }
        }
        install(HttpTimeout) {
            // timeout config
            requestTimeoutMillis = 60000
            socketTimeoutMillis = 60000
        }
    }

    companion object {
        private lateinit var server: NettyApplicationEngine
        private val logger = LogManager.getLogger()

        @BeforeAll
        @JvmStatic
        fun setup() {
            logger.info("Starting server")
            server =
                embeddedServer(Netty, port = getProperty("deployment.port").toInt()) { module(testing = true) }.start(
                    wait = false
                )
        }

        @AfterAll
        @JvmStatic
        fun teardown() {
            logger.info("Terminating server")
            server.stop(1000, 10000)
            logger.info("Server terminated")
        }
    }

    @Test
    fun `checkout for empty request body`() {
        val endpoint = "http://localhost:" + getProperty("deployment.port").toInt() + "/checkout"
        val testRequestBody = ""
        val expectedJson = "{ \"price\" : 0 }"
        val exception = Assertions.assertThrows(ClientRequestException::class.java) {
            generalTest(endpoint, testRequestBody, expectedJson)
        }
        assertEquals(
            exception.message,
            "Client request(http://localhost:3000/checkout) invalid: 400 Bad Request. Text: \"Request body should contain list of product ids for checkout\""
        )
        assertEquals(exception.response.status, HttpStatusCode.BadRequest)
    }

    @Test
    fun `checkout for invalid request body`() {
        val endpoint = "http://localhost:" + getProperty("deployment.port").toInt() + "/checkout"
        val testRequestBody = "xxx"
        val expectedJson = "{ \"price\" : 0 }"
        val exception = Assertions.assertThrows(ClientRequestException::class.java) {
            generalTest(endpoint, testRequestBody, expectedJson)
        }
        assertEquals(exception.message, "Client request(http://localhost:3000/checkout) invalid: 400 Bad Request. Text: \"java.util.Collections\$SingletonList cannot be cast to java.util.ArrayList\"")
        assertEquals(exception.response.status, HttpStatusCode.BadRequest)
    }

    @Test
    fun `checkout for invalid products`() {
        val endpoint = "http://localhost:" + getProperty("deployment.port").toInt() + "/checkout"
        val testRequestBody = "{\"test1\", \"test2\"}"
        val expectedJson = "{ \"price\" : 0 }"
        generalTest(endpoint, testRequestBody, expectedJson)
    }

    @Test
    fun `checkout for no offer`() {
        val endpoint = "http://localhost:" + getProperty("deployment.port").toInt() + "/checkout"
        val testRequestBody = "{\"001\", \"002\", \"001\", \"004\", \"003\"}"
        val expectedJson = "{ \"price\" : 360 }"
        generalTest(endpoint, testRequestBody, expectedJson)
    }

    @Test
    fun `checkout for single offer`() {
        val endpoint = "http://localhost:" + getProperty("deployment.port").toInt() + "/checkout"
        val testRequestBody = "{\"001\", \"002\", \"001\", \"004\", \"003\", \"001\"}"
        val expectedJson = "{ \"price\" : 360 }"
        generalTest(endpoint, testRequestBody, expectedJson)
    }

    @Test
    fun `checkout for multiple offer`() {
        val endpoint = "http://localhost:" + getProperty("deployment.port").toInt() + "/checkout"
        val testRequestBody = "{\"001\", \"002\", \"001\", \"004\", \"002\", \"003\", \"001\"}"
        val expectedJson = "{ \"price\" : 400 }"
        generalTest(endpoint, testRequestBody, expectedJson)
    }

    private fun generalTest(endpoint: String, requestBody: String, expectedJson: String) {
        val res = runBlocking {
            return@runBlocking client.post<String>(endpoint) {
                header("Connection", "close")
                body = requestBody
            }
        }

        JSONAssert.assertEquals(expectedJson, res, JSONCompareMode.STRICT)
    }
}
