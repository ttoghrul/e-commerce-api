package com.ecommerce.api.controller

import com.ecommerce.api.model.TotalPrice
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class CommerceControllerTest {
    private val testCommerceController = CommerceController()

    @Test
    fun `checkout for all invalid products`() {
        val testList = listOf("xyz", "abcd")
        assertEquals(TotalPrice(0), testCommerceController.checkout(testList))
    }

    @Test
    fun `checkout for single invalid product`() {
        val testList = listOf("001", "abcd")
        assertEquals(TotalPrice(100), testCommerceController.checkout(testList))
    }

    @Test
    fun `checkout for no products`() {
        val testList = ArrayList<String>()
        assertEquals(TotalPrice(0), testCommerceController.checkout(testList))
    }

    @Test
    fun `checkout for no offers`() {
        val testList = listOf("001", "002", "003", "004", "001")
        assertEquals(TotalPrice(360), testCommerceController.checkout(testList))
    }

    @Test
    fun `checkout for single offer for single product`() {
        val testList = listOf("001", "002", "003", "004", "001", "001")
        assertEquals(TotalPrice(360), testCommerceController.checkout(testList))
    }

    @Test
    fun `checkout for multiple offer for single product`() {
        val testList = listOf("001", "002", "003", "004", "001", "001", "001", "001", "001", "001")
        assertEquals(TotalPrice(660), testCommerceController.checkout(testList))
    }

    @Test
    fun `checkout for single offer for multiple products`() {
        val testList = listOf("001", "002", "002", "002", "003", "004", "001", "001")
        assertEquals(TotalPrice(480), testCommerceController.checkout(testList))
    }

    @Test
    fun `checkout for multiple offers for multiple products`() {
        val testList = listOf("001", "002", "002", "002", "002", "003", "004", "001", "001", "001", "001", "001", "001")
        assertEquals(TotalPrice(820), testCommerceController.checkout(testList))
    }
}
