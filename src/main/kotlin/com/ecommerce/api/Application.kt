package com.ecommerce.api

import com.ecommerce.api.controller.CommerceController
import com.ecommerce.api.exception.InvalidRequestException
import com.ecommerce.api.exception.MissingRequestBodyException
import com.ecommerce.api.model.TotalPrice
import com.typesafe.config.ConfigFactory
import de.nielsfalk.ktor.swagger.SwaggerSupport
import de.nielsfalk.ktor.swagger.ok
import de.nielsfalk.ktor.swagger.post
import de.nielsfalk.ktor.swagger.responds
import de.nielsfalk.ktor.swagger.version.shared.Information
import de.nielsfalk.ktor.swagger.version.v2.Swagger
import de.nielsfalk.ktor.swagger.version.v3.OpenApi
import io.ktor.application.* // ktlint-disable no-wildcard-imports
import io.ktor.config.* // ktlint-disable no-wildcard-imports
import io.ktor.features.* // ktlint-disable no-wildcard-imports
import io.ktor.gson.* // ktlint-disable no-wildcard-imports
import io.ktor.http.* // ktlint-disable no-wildcard-imports
import io.ktor.locations.* // ktlint-disable no-wildcard-imports
import io.ktor.response.* // ktlint-disable no-wildcard-imports
import io.ktor.routing.* // ktlint-disable no-wildcard-imports
import io.ktor.server.engine.* // ktlint-disable no-wildcard-imports
import io.ktor.server.netty.* // ktlint-disable no-wildcard-imports
import org.apache.logging.log4j.LogManager
import java.time.Duration
import kotlin.collections.set

private var config: HoconApplicationConfig? = null
private val commerceController = CommerceController()

fun getProperty(property: String): String {
    if (config == null) config = HoconApplicationConfig(ConfigFactory.load())
    return config!!.property("ktor.$property").getString()
}

@Location("/checkout") class checkout()

fun main(args: Array<String>) {
    val server = embeddedServer(Netty, port = getProperty("deployment.port").toInt()) { module() }
    server.start(wait = true)
}

@kotlin.jvm.JvmOverloads
fun Application.module(testing: Boolean = false) {
    val logger = LogManager.getLogger()
    install(CORS) {
        method(HttpMethod.Options)
        method(HttpMethod.Get)
        method(HttpMethod.Post)
        method(HttpMethod.Put)
        method(HttpMethod.Delete)
        method(HttpMethod.Patch)
        header(HttpHeaders.AccessControlAllowHeaders)
        header(HttpHeaders.ContentType)
        header(HttpHeaders.AccessControlAllowOrigin)
        allowCredentials = true
        anyHost()
        maxAge = Duration.ofDays(1)
    }
    install(DefaultHeaders)
    install(Compression)
    install(CallLogging)
    install(ContentNegotiation) {
        gson {
            setPrettyPrinting()
        }
    }
    install(Locations)
    install(StatusPages) {
        exception<MissingRequestBodyException> { ex ->
            call.respond(HttpStatusCode.BadRequest, ex.message ?: "Error with the request $ex")
        }
        exception<InvalidRequestException> { ex ->
            call.respond(HttpStatusCode.BadRequest, ex.message ?: "Error with the request $ex")
        }
    }
    install(SwaggerSupport) {
        forwardRoot = true
        val information = Information(
            version = "0.1.0",
            title = "E-Commerce API for Java Coding Challenge"
        )
        swagger = Swagger().apply {
            info = information
            definitions["uri"] = "/swagger"
        }
        openApi = OpenApi().apply { info = information }
    }

    routing {
        post<checkout, String>("Generating total price for checkout".responds(ok<TotalPrice>())) { _, entity ->
            run {
                if (entity.isEmpty()) {
                    throw MissingRequestBodyException("Request body should contain list of product ids for checkout")
                }
                try {
                    val productIdsForPurchase: ArrayList<String> =
                        entity.replace("[{\"\\s}]".toRegex(), "").split(",")
                            .toList() as ArrayList<String>
                    logger.debug("Checkout endpoint is called for product ids : $productIdsForPurchase")
                    val totalPrice = commerceController.checkout(productIdsForPurchase)
                    call.respond(HttpStatusCode.OK, totalPrice)
                } catch (e: Exception) {
                    throw InvalidRequestException(e.message)
                }
            }
        }
    }
}
