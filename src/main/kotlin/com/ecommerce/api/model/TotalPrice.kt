package com.ecommerce.api.model

class TotalPrice(private var price: Int) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as TotalPrice

        if (price != other.price) return false

        return true
    }

    override fun hashCode(): Int {
        return price
    }

    override fun toString(): String {
        return "TotalPrice(price=$price)"
    }
}
