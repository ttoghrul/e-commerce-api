package com.ecommerce.api.controller

import com.ecommerce.api.model.TotalPrice
import org.apache.logging.log4j.LogManager

class CommerceController {

    private val prices = mapOf("001" to 100, "002" to 80, "003" to 50, "004" to 30)
    private val offers = mapOf("001" to Pair(3, 200), "002" to Pair(2, 120))
    private val logger = LogManager.getLogger()

    fun checkout(productIds: List<String>): TotalPrice {
        var totalPrice = 0
        val productIdsGrouped = HashMap<String, Int>()

        for (id in productIds) {
            productIdsGrouped[id] = productIdsGrouped.getOrDefault(id, 0) + 1
        }

        for (product in productIdsGrouped) {
            if (offers.containsKey(product.key)) {
                val eligibleOfferCount = offers[product.key]
                if (eligibleOfferCount != null) {
                    totalPrice += (product.value / eligibleOfferCount.first) * eligibleOfferCount.second
                    productIdsGrouped[product.key] = product.value % eligibleOfferCount.first
                }
            }
            totalPrice += product.value * prices.getOrDefault(product.key, 0)
        }

        val responseWithPrice = TotalPrice(totalPrice)
        logger.debug("checkout result: $responseWithPrice")
        return responseWithPrice
    }
}
