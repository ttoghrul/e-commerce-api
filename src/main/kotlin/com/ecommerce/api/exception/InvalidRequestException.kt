package com.ecommerce.api.exception

class InvalidRequestException(message: String?) : Exception(message)
