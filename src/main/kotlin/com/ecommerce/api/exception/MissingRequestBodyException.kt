package com.ecommerce.api.exception

import java.lang.Exception

class MissingRequestBodyException(message: String) : Exception(message)
