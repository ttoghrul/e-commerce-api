import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm") version "1.4.20"
    application
}

buildscript {
    dependencies {
        classpath("org.jetbrains.kotlin:kotlin-gradle-plugin:1.4.20")
    }
}

application.mainClassName = "io.ktor.server.netty.EngineMain"

val log4j2Version: String by project
val junitVersion: String by project
val ktorVersion: String by project
val kotlinVersion: String by project
val skyScreamerVersion: String by project

repositories {
    mavenLocal()
    jcenter()
    maven(url = "https://jitpack.io")
    mavenCentral()
}

val ktlint: Configuration by configurations.creating

configurations.all {
    resolutionStrategy {
        cacheChangingModulesFor(0, "seconds")
        force("org.apache.logging.log4j:log4j-slf4j-impl:$log4j2Version")
    }
}

project.group = "com.ecommerce.api"
project.version = "0.1.0"

tasks {
    withType<KotlinCompile> {
        kotlinOptions {
            jvmTarget = "1.8"
            languageVersion = "1.4"
        }
    }

    withType<Test> {
        useJUnitPlatform()
    }

    withType<Jar> {
        manifest {
            attributes("Main-Class" to "com.ecommerce.api.ApplicationKt")
        }
        from({ configurations.compile.get().map { if (it.isDirectory) it else zipTree(it) } })
    }

    register<JavaExec>("ktlint") {
        description = "Check Kotlin code style."
        group = "ktlint"

        classpath = ktlint
        main = "com.pinterest.ktlint.Main"
        args("src/**/*.kt")
    }

    register<JavaExec>("ktlintFormat") {
        description = "Fix Kotlin code style deviations."
        group = "ktlint"

        classpath = ktlint
        main = "com.pinterest.ktlint.Main"
        args("-F", "src/**/*.kt")
    }

    "check" {
        dependsOn("ktlint")
    }
}

dependencies {

    implementation(kotlin("stdlib-jdk8"))
    implementation("org.apache.logging.log4j", "log4j-api", log4j2Version)
    implementation("org.apache.logging.log4j", "log4j-core", log4j2Version)
    implementation("org.apache.logging.log4j", "log4j-slf4j-impl", log4j2Version)

    implementation("io.ktor", "ktor-server-netty", ktorVersion)
    implementation("io.ktor", "ktor-client-core", ktorVersion)
    implementation("io.ktor", "ktor-client-core-jvm", ktorVersion)
    implementation("io.ktor", "ktor-gson", ktorVersion)
    implementation("io.ktor", "ktor-client-jackson", ktorVersion)
    implementation("io.ktor", "ktor-client-okhttp", ktorVersion)
    implementation("com.github.nielsfalk", "ktor-swagger", "0.7.0")

    testImplementation("org.skyscreamer", "jsonassert", skyScreamerVersion)
    testImplementation("org.junit.jupiter", "junit-jupiter-api", junitVersion)
    testRuntimeOnly("org.junit.jupiter", "junit-jupiter-engine", junitVersion)

    ktlint("com.pinterest", "ktlint", "0.39.0")
}
