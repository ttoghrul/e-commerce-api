# E-Commerce API

E-Commmerce API is a RESTful API that does checkout for requested watch products list (built as part of Java Coding Challenge).     

E-Commerce API takes into account available offers as well as price of the items, mentioned in watch catalogue below

| **Watch ID** | **Watch Name** | **Unit Price** | **Discount** |
|--------------|----------------|----------------|--------------|
| 001          | Rolex          | 100            | 3 for 200    |
| 002          | Michael Kors   | 80             | 2 for 120    |
| 003          | Swatch         | 50             |              |
| 004          | Casio          | 30             |              |

## Request and response examples
API currently has just a single endpoint **/checkout** that takes no request parameters and expect products to be provided as a list of ids in request body.  

Below is the example request to E-Commerce API

```
POST http://localhost:8080/checkout
```
```
# Headers
Accept: application/json
Content-Type: application/json
```
```
# Body
[
 "001",
 "002",
 "001",
 "004",
 "003"
]
```

and expected response

```
# Headers
Content-Type: application/json
```
```
# Body
{ "price": 360 }
```
## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.  

### Prerequisites

* Any Operating System
* Java 8+

### Installing

Below are steps to get a development env running

```
git clone https://ttoghrul@bitbucket.org/ttoghrul/e-commerce-api.git
```
```
gradlew clean build
```
```
gradlew run (or if in IDEA - right-click on Application.kt and choose Run)
```
As a result E-Commerce API will be accessible at **localost** and the port number configured in **application.conf** file (default value is 3000)

E-Commerce API has Swagger support which means that if opened in browser (http://localhost:3000) - one will be able to see Swagger Page with ability to test API and see endpoint and parameters description 

## Running the tests

E-Commerce API has unit tests, end-to-end tests and coding style tests

### Unit tests

Unit tests checks functionality of individual classes and methods  
 
Unit tests can be run using command below

```
gradlew clean tests
```

### End-to-end tests

End-to-end does blackbox testing by calling E-Commerce API endpoint for various use-cases  

It contains both happy path and negative scenarios and can be run using command below  

```
gradlew test --tests com.ecommerce.api.APIRunnerTest
```

### Coding style tests

Coding style tests using Klint

```
gradlew clean ktlint
```

## Deployment

In order to build and deploy the version of E-Commerce API steps below should be followed

```
gradlew clean assemble
```

Go to below folder in e-commerce-api source folder
```
build/distributions/e-commerce-api-<version number> (current version number is 0.1.0)
```

There should be **.tar** and **.zip** version of e-commerce-api app  
You can run any of them by extracting the archive file and running following command in source unarchived folder 

```
e-commerce-api (for Linux)
```
```
./e-commerce-api.bat (for Windows) 
```

## Built With

* [Kotlin](https://kotlinlang.org/) - Main programming language used
* [Gradle](https://gradle.org/) - Dependency Management
* [Ktor](https://ktor.io/) - REST API framework
* [Swagger](https://swagger.io/) -  Interface Description Language for REST API description
* [JUnit 5](https://junit.org/junit5/) - Testing framework

## Versioning

We use [SemVer](http://semver.org/) for versioning. 

## Authors

* [**Toghrul Taghiyev**](mailto:ttoghrul@gmail.com)